/**
 * STOCKHOLM UNIVERSITY - CPROG - FALL TERM 2020 - ASSIGNMENT 3
 * @author_Dimitrios_Mavromatis_dima1894
 * Wrote the implementations of the constructor overloading and other functions
 */

// Labb3, Datum.cpp – definitioner av icke-triviala medlemsfunktioner

#include <iostream>
#include <string>
#include "Datum.h"

// Initialisera statisk medlem
// (första elementet används inte i denna lösning!)
const std::array< unsigned int, 13 > Datum::ANT_DAGAR_PER_MAANAD = { 0,
                                                                     31,28,31,30,
                                                                     31,30,31,31,
                                                                     30,31,30,31 };

// Default constructor
Datum::Datum() : year(2000), month(1), day(1){
}


// Konstruktor
Datum::Datum( int year, int month, int day  )
{
    set_date( year, month, day);
}

void Datum::set_date( int yy, int mm, int dd )
{
    if (yy > 2100) {
        throw std::invalid_argument("Error: year cannot exceed 2100");
    }
    if (yy < 2000) {
        throw std::invalid_argument("Error: year cannot deceed 2100");
    }
    year = yy;
    if (mm > 12) {
        throw std::invalid_argument("Error: month cannot exceed 12");
    }
    if (mm < 1) {
        throw std::invalid_argument("Error: year cannot deceed 1");
    }
    month = mm;
    if (is_skott_aar(yy) && month == 2 && dd > (ANT_DAGAR_PER_MAANAD[month] + 1)) {
        throw std::invalid_argument("Too many days given for the month provided");
    }
    if (month != 2 && dd > (ANT_DAGAR_PER_MAANAD[month])) {
        throw std::invalid_argument("Too many days given for the month provided");
    }
    day = dd;
}

int Datum::get_year() const {
    int temp(year);
    return temp;
}

int Datum::get_month() const {
    int temp(month);
    return temp;
}

int Datum::get_day() const{
    int temp(day);
    return temp;
}

// Denna medlemsfunktion är tänkt att lägga till 1 dag till befintligt datum.
// Om sista dagen i månaden, så byt månad.
//     Om skottår, så ta hänsyn till att februari(månad 2) har 29 dagar.
// Om sista dagen på året, så byt år.
void Datum::step_one_day()
{
    // Dagen är inte sista dagen i månaden!

    /**
     * This function would work perfectly fine.
     * I decided not to use it since I implemented my own that take in a number of days
     * That is the step_number_of_days function. Works as well with one day increments.
     * It is arguably better to use this one with the postfix and prefix ++ operators but then again
     * I found it more interesting and challenging to write my own.
     */

    if ( !end_of_month( day ) )
        ++day;
    else
    if ( month < 12 )
    {
        ++month;
        day = 1;
    }
    else
    {
        ++year;
        month = 1;
        day = 1;
    }
}

int Datum::get_month_max_days() {
    if (month == 2 && is_skott_aar(year)) {
        return (ANT_DAGAR_PER_MAANAD[month] + 1);
    }
    return ANT_DAGAR_PER_MAANAD[month];
}

/*
 * This function works similarly with step one day but instead it works with a given number of days.
 * That is as long as the year does not exceed 2100. The method uses a r_day variable (remaining days of each given month)
 * in order to subtract the day left in the month from the number given if that number exceeds that.
 * If it exceeds it, that means that we needs to change month. If the months is 12 then the year increments.
 * That is done in a while loop and goes until the number given is 0.
 * Note that if (or when in the case the while loop runs more than once) the number given does not exceed the remaining
 * days of the month then the day is set to the current day plus the number and the number set to zero.
 * That is what this function does as well as performing leap year and other checks.
 */
void Datum::step_number_days(int number)
{
    int r_days; // remaining days in a month to be subtracted from the number
    while (number > 0) {
        r_days = get_month_max_days();
        if (number >= r_days) {
            number_bigger_than_remaining_days(r_days, number);
        } //
        else {
            number_smaller_than_remaining_days(r_days, number);
        }
    } //while loop end
}

void Datum::number_smaller_than_remaining_days(int r_days, int &number) {
    r_days = get_month_max_days() - day;
    if (r_days - number >= 0) { // If we increment the day with number and we do NOT reach the end of the month
        day += number;
        number = 0;
    }
    else {
        day = 1;
        if (month == 12) { // If we increment the day with number and we DO reach the end of the LAST month
            month = 1;
            year++;
            if (year > 2100) {
                throw std::invalid_argument("Error: year cannot deceed 2100");
            }
        } else { // If we increment the day with number and we DO reach the end of the month but NOT the last month
            r_days = get_month_max_days() - day;
            number -= r_days;
            if (number < 0) {
                number = 0;
            }
        }
    }
}

void Datum::number_bigger_than_remaining_days(int r_days, int &number) {
    number -= r_days;
    day = 1;
    if (month == 12) {
        month = 1;
        year++;
        if (year > 2100) {
            throw std::invalid_argument("Error: year cannot deceed 2100");
        }
    } else {
        month++;
    }
}

/*Todo
 * .
 * Not to forget to jump the zero index element in every iteration øf the maxDaysMonth array.
 * Set the given days into a buffer.
 * check if the buffer exceeds the amount of days left within the current month.
 * If it doesn't then add the days into the month and subtract them from the buffer.
 * If the buffer is 0 then exit the loop.
 * Else if the buffer exceed the number of days of the current month go the next month,
 * and subtract the remaining days of the current months from the buffer.
 * If again the buffer is not zero add the
 */

// Returnera true om yy(year) är skottår!
bool Datum::is_skott_aar( int yy )
{
    if ( yy % 400 == 0 ||
         ( yy % 100 != 0 && yy % 4 == 0 ) )
        return true;
    else
        return false;
}

// Är det sista dagen (dd) i månaden?
bool Datum::end_of_month( int dd ) const
{
    if ( month == 2 && is_skott_aar( year ) )
        return dd == 29;
    else
        return dd == ANT_DAGAR_PER_MAANAD[ month ];
}

// operator<<
std::ostream &operator<<( std::ostream &output, const Datum &d )
{
    //Todo
    // OBS. Glöm inte att modifiera vad som skrivs ut!
    // Ett datumobjekt som representerar datum (2020, 2, 20)
    // skall skrivas ut på formen "20 Februari 2020"
    std::string str;

    switch (d.month)
    {
        case 1:
        str= "January";
        break;

        case 2:
        str= "February";
        break;

        case 3:
        str= "March";
        break;

        case 4:
        str= "April";
        break;

        case 5:
        str= "May";
        break;

        case 6:
        str= "June";
        break;

        case 7:
        str= "July";
        break;

        case 8:
        str= "August";
        break;

        case 9:
        str= "September";
        break;

        case 10:
        str= "October";
        break;

        case 11:
        str= "November";
        break;

        case 12:
        str= "December";
        break;
    }
    output << d.day << ' ' << str<< ' ' << d.year;
    return output;

}

Datum& Datum::operator+= (int other) {
    step_number_days(other);
    return *this;
}

const Datum& Datum::operator++() { // Prefix
    return *this += 1;
}

const Datum Datum::operator++(int) { // Postfix
    Datum temp(*this);
    *this += 1;
    return temp;
}

const Datum Datum::operator+(int right) const{ // Left side Datum right side number
    Datum temp(*this);
    temp += right;
    return temp;
}

const Datum operator+(int left, Datum right) { // Left side number right side Datum
    Datum temp(right);
    temp += left;
    return temp;
}

bool operator>(const Datum left, const Datum right) {
    return !(left < right);
}

bool operator<(const Datum left, const Datum right) {
    //First level of comparison - Year
    if (left.get_year() < right.get_year()) {
        return true;
    }
    if(left.get_year() > right.get_year()) {
        return false;
    }
    //Second level of comparison - Month
    if(left.get_month() < right.get_month()) {
        return true;
    }
    if(left.get_month() > right.get_month()) {
        return false;
    }
    //Third level of comparison - Day
    if(left.get_month() < right.get_day()) {
        return true;
    }
    if(left.get_month() > right.get_day()) {
        return false;
    }
    return false;
}

bool operator==(const Datum left, const Datum right) {
    return
    left.get_year() == right.get_year() &&
    left.get_month() == right.get_month() &&
    left.get_day() == right.get_day();
}

bool operator!=(const Datum left, const Datum right) {
    return !(left == right);
}

bool operator<=(const Datum left, const Datum right) {
    if (left == right) {
        return true;
    }
    if (left < right) {
        return true;
    }
    return false;
}

bool operator>=(const Datum left, const Datum right) {
    if (left == right) {
        return true;
    }
    if (left > right) {
        return true;
    }
    return false;
}

