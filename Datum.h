/**
 * STOCKHOLM UNIVERSITY - CPROG - FALL TERM 2020 - ASSIGNMENT 3
 * @author_Dimitrios_Mavromatis_dima1894
 * Wrote the implementations of the constructor overloading and other functions
 */

// Labb3, Datum.h – klassdefinition

#ifndef DATUM_H
#define DATUM_H

#include <array>
#include <iostream>

class Datum
{
    unsigned int year;
    unsigned int month;
    unsigned int day;

    // Deklaration av statisk privat medlem, "ant_dagar per månad"
    static const std::array< unsigned int, 13 > ANT_DAGAR_PER_MAANAD;

    // Function given
    void step_one_day(); // Öka datum med en dag
    static bool is_skott_aar( int ); // Är det skottår?
    bool end_of_month( int ) const; // Är dagen den sista i månaden?

    // Function I wrote
    int get_month_max_days();
    void step_number_days(int number);
    void number_bigger_than_remaining_days(int r_days, int &buffer);
    void number_smaller_than_remaining_days(int r_days, int &buffer);

    // Operator overloading
    friend std::ostream &operator<<( std::ostream &, const Datum & );

public:
    Datum(); // Default constructor
    Datum( int, int, int );
    void set_date( int, int, int ); // set year, month, day

    // Operator overloading
    Datum& operator+= (int other);
    const Datum& operator++(); // Prefix
    const Datum operator++(int); // Postfix
    const Datum operator+(int right) const;

    int get_year() const;
    int get_month() const;
    int get_day() const;
};

const Datum operator+(int left, Datum right);
bool operator<(const Datum left, const Datum right);
bool operator>(const Datum left, const Datum right);
bool operator==(const Datum left, const Datum right);
bool operator!=(const Datum left, const Datum right);
bool operator<=(const Datum left, const Datum right);
bool operator>=(const Datum left, const Datum right);

#endif

//Todo handledning questions to ask:
// What is the const at the end of the signature?